﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class HandManager : MonoBehaviour {


    // Singleton
    private static HandManager _instance;
    public static HandManager Instance { get { return _instance;  } }

    /* Card positioning on Hand */
    public Vector3 centerPos;
    public float radius = 1f;
    public float stepAngle = 10f;
    public float startAngle = 0f;
    /*****************************************/

    public delegate void CardObjectHandler();
    public event CardObjectHandler CardMoved;

    // Card gameobjects
    List<GameObject> cardGOs = new List<GameObject>();

    // Hand model that this controller uses
    Hand myHand;



    //Hand.HandCardChangedHandler handDelegate;


    void Start()
    {
        myHand = new Hand();
        //handDelegate = new Hand.HandCardChangedHandler(this.OnCardAdded);
        myHand.HandCardAdded += OnCardAdded;
        InputManager.Instance.DragStarted += OnDragStarted;
        InputManager.Instance.DragEnded += OnDragEnded;
    }


  
    void OnDragStarted(GameObject cardGO)
    {
        iTween.Stop(cardGO);
        //cardGO.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        iTween.ScaleTo(cardGO, iTween.Hash("x", 0.8f, "y", 0.8f, "z", 0.8f, "time", 0.5f));
        iTween.RotateTo(cardGO, iTween.Hash("x", 90, "y", 0, "z", 0, "time", 0.5f));
    }




    // TODO: Improve card swap
    void OnDragEnded(GameObject cardGO)
    {
        iTween.Stop(cardGO);
        iTween.ScaleTo(cardGO, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f));

        // Find the index of the card that we dragged closest
        int newIndex = 0;
        for (int i = 0; i < cardGOs.Count; i++)
        {
            if (cardGO.transform.position.x > cardGOs[i].transform.position.x)
            {
                newIndex = i;
            }
        }

        // Move gameobject to its new position
        cardGOs.Remove(cardGO);
        cardGOs.Insert(newIndex, cardGO);


       
        // Recalculate card positions and play animations
        for (int i = 0; i < cardGOs.Count; i++)
        {
            Vector3[] cardPosRot = reCalculateHoldingHandPosition(cardGOs[i]);
            iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPosRot[0].x, "y", cardPosRot[0].y, "z", cardPosRot[0].z, "delay", 0.1f));
            iTween.RotateTo(cardGOs[i], iTween.Hash("y", cardPosRot[1].y, "z", 0, "delay", 0.1f));
        }


        /*
        float yRot = reCalculateHoldingHandPosition(cardGO)[1].y;
        iTween.RotateTo(cardGO, iTween.Hash("x", 90, "y", yRot, "z", 0, "time", 0.5f));*/
    }

    void OnCardAdded(Card card)
    {

        GameObject cardGO = GameManager.Instance.CardGameObjectDict[card];
        cardGOs.Add(cardGO);
        Vector3[] cardPosRot;


        /* Initial animation */
        iTween.MoveTo(cardGO, iTween.Hash("x", 0, "y", 6f, "z", -1f, "time", 1f));
        iTween.RotateTo(cardGO, iTween.Hash("x", 90, "time", 1f));

        /* Move gameobject to its expected position */
        cardPosRot = reCalculateHoldingHandPosition(cardGO);
        iTween.MoveTo(cardGO, iTween.Hash("x", cardPosRot[0].x, "y", cardPosRot[0].y, "z", cardPosRot[0].z, "delay", 1.5f));
        iTween.RotateTo(cardGO, iTween.Hash("x", 90, "y", cardPosRot[1].y, "z", 0, "delay", 1.5f));

        /* Wait till animations finish and activate card for inputs */
        StartCoroutine(switchInteractable(cardGO));

    }

    IEnumerator switchInteractable(GameObject cardGO)
    {
        yield return new WaitForSeconds(2.3f);
        cardGO.GetComponent<CardInfo>().interactable = true;
        cardGO.GetComponent<CardInfo>().inHand = true;
    }

 
    public void AddCardToHand(Card card)
    {
        myHand.AddCard(card);
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else {
            _instance = this;
        }
    }

    public void SortHandBySequence()
    {
        HandGroup result = myHand.SortByCompleteSequences(myHand.cards);
        List<Card> sorted = result.sorted;
        sorted.AddRange(result.unsorted);

        myHand.cards = sorted;


        for (int i = 0; i < cardGOs.Count; i++)
        {
            cardGOs[i] = GameManager.Instance.CardGameObjectDict[myHand.cards[i]];
        }

        for (int i = 0; i < cardGOs.Count; i++)
        {
            Vector3[] cardPosRot = reCalculateHoldingHandPosition(cardGOs[i]);
            //iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPos.x, "y", cardPos.y, "z", cardPos.z, "delay", i * 0.05f));
            iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPosRot[0].x, "y", cardPosRot[0].y, "z", cardPosRot[0].z, "delay", 0.1f));
            iTween.RotateTo(cardGOs[i], iTween.Hash("y", cardPosRot[1].y, "z", 0, "delay", 0.1f));
        }
        if (CardMoved != null) CardMoved();

    }


    public void SortHandByGroup()
    {
        HandGroup result = myHand.SortByCompleteGroups(myHand.cards);
        List<Card> sorted = result.sorted;
        sorted.AddRange(result.unsorted);

        myHand.cards = sorted;


        for (int i = 0; i < cardGOs.Count; i++)
        {
            cardGOs[i] = GameManager.Instance.CardGameObjectDict[myHand.cards[i]];
        }

        for (int i = 0; i < cardGOs.Count; i++)
        {
            Vector3[] cardPosRot = reCalculateHoldingHandPosition(cardGOs[i]);
            //iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPos.x, "y", cardPos.y, "z", cardPos.z, "delay", i * 0.05f));
            iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPosRot[0].x, "y", cardPosRot[0].y, "z", cardPosRot[0].z, "delay", 0.1f));
            iTween.RotateTo(cardGOs[i], iTween.Hash("y", cardPosRot[1].y, "z", 0, "delay", 0.1f));
        }
        if (CardMoved != null) CardMoved();
    }

    public void SortHandSmartly()
    {
        HandGroup result = myHand.SmartSort(myHand.cards);
        List<Card> sorted = result.sorted;
        
        myHand.cards = sorted;


        for (int i = 0; i < cardGOs.Count; i++)
        {
            cardGOs[i] = GameManager.Instance.CardGameObjectDict[myHand.cards[i]];
        }

        for (int i = 0; i < cardGOs.Count; i++)
        {
            Vector3[] cardPosRot = reCalculateHoldingHandPosition(cardGOs[i]);
            //iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPos.x, "y", cardPos.y, "z", cardPos.z, "delay", i * 0.05f));
            iTween.MoveTo(cardGOs[i], iTween.Hash("x", cardPosRot[0].x, "y", cardPosRot[0].y, "z", cardPosRot[0].z, "delay", 0.1f));
            iTween.RotateTo(cardGOs[i], iTween.Hash("y", cardPosRot[1].y, "z", 0, "delay", 0.1f));
        }

        if (CardMoved != null) CardMoved();
    }
  

    // Returns the expected position and rotation for a gameobject in the hand 
    public Vector3[] reCalculateHoldingHandPosition(GameObject cardGO)
    {
        Vector3[] transformAndPosition = new Vector3[2];
        startAngle = -30f;
        float currentAngle = startAngle;
        float nextY = 0;
        foreach (GameObject card in cardGOs)
        {
            if(card == cardGO)
            {
                Vector3 nextPosition = centerPos;
                nextPosition.x += Mathf.Sin(currentAngle * Mathf.PI / 180) * radius;
                nextPosition.z += Mathf.Cos(currentAngle * Mathf.PI / 180) * radius;
                nextPosition.y += nextY;

                transformAndPosition[0] = nextPosition;
                transformAndPosition[1] = new Vector3(cardGO.transform.localEulerAngles.x, currentAngle, cardGO.transform.localEulerAngles.z);
            }
            nextY += 0.01f;
            currentAngle += stepAngle;

        }
        return transformAndPosition;
    }

    void OnDestroy()
    {
        myHand.HandCardAdded -= OnCardAdded;
        InputManager.Instance.DragStarted -= OnDragStarted;
        InputManager.Instance.DragEnded -= OnDragEnded;
    }

}
