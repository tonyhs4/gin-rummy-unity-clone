﻿using System;

public class Card
{
    private Suit _suit;
    private Value _value;

    public Suit Suit
    {
        get { return _suit; }
    }

    public Value Value
    {
        get { return _value; }
    }

    // Constructor which requires single integer parameter between 0 and 52 to construct a card object
    // E.g. cardNumber 39 will be an Ace of Clubs, cardNumber 43 will be a Five of Clubs
    // E.g. cardnumber 0 will be Ace of Spades, cardnumber 33 will be Eight of Diamonds etc.
    public Card(int cardNumber)
    {
        // check if given number between legal boundaries
        if (cardNumber < 0 || cardNumber >= 52)
            throw new ArgumentOutOfRangeException();

        _value = (Value)((cardNumber % 13) + 1);
        _suit = (Suit)(cardNumber / 13);
    }

    // Constructor which requires both suit and value parameters to create a card object
    // E.g. suit Suit.Spades and value Value.Eight will return a card object which is Eight of Spades
    public Card(Suit suit, Value value)
    {
        _suit = suit;
        _value = value;
    }

    public override string ToString()
    {
        return Value + " of " + Suit;
    }

    public int toCardNumber()
    {
        return ((int)Suit * 13) + ((int)Value - 1);
    }
}

