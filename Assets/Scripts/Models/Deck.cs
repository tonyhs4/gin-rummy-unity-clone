﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
public class Deck
{
    private const int START_DECK_SIZE = 52;
    public List<Card> cards;

    System.Random _random = new System.Random();


    public delegate void DeckHandler();
    public event DeckHandler CardDrawn;

    public Deck()
    {
        cards = new List<Card>();
        Initialize();
    }

    // Fill the deck with cards in order, from Ace of Spades through King of Clubs
    public void Initialize()
    {
        for (int i = 0; i < START_DECK_SIZE; i++)
        {
            cards.Add(new Card(i));
        }
    }

    public Card DrawSpecificCard(int cardNum)
    {

        foreach (Card card in cards)
        {
            if(card.toCardNumber() == cardNum)
            {
                if (CardDrawn != null) CardDrawn();
                cards.Remove(card);
                return card;
            }
        }
        throw new KeyNotFoundException("DrawSpecificCard failed.");
    }


    // Shuffle the deck using Fisher-Yates shuffle algorithm
    public void Shuffle()
    {
        for (int i = cards.Count-1; i > 0; i--)
        {
            int randomIndexToSwap = _random.Next(i);
            Card tmp = cards[randomIndexToSwap];
            cards[randomIndexToSwap] = cards[i];
            cards[i] = tmp;
        }
    }

    // Draw first card from top of the deck
    public Card DrawCard()
    {


        if (cards.Count == 0)
        {
            return null;
        }
        if (CardDrawn != null) CardDrawn();
        Card cardToDraw = cards[0];
        cards.RemoveAt(0);
        return cardToDraw;
    }


    // Draw first N cards , useful to draw starting hand.
    public List<Card> DrawCards(int amount)
    {
        if (cards.Count < amount)
        {
            return null;
        }
        List<Card> startingHand = cards.Take(amount).ToList();
        cards.RemoveRange(0, amount);
        return startingHand;
    }
}

