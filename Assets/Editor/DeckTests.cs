﻿using NUnit.Framework;

public class DeckTests
{
    [Test]
    public void ShouldReturn52AsTheSizeOfDeck()
    {
        Deck testDeck = new Deck();
        Assert.AreEqual(testDeck.cards.Count, 52);
    }

    [Test]
    public void ShouldReturnAceOfSpadesAsTheFirstCardOfTheDeck()
    {
        Deck testDeck = new Deck();
        Assert.AreEqual(testDeck.cards[0].Value, Value.Ace);
        Assert.AreEqual(testDeck.cards[0].Suit, Suit.Spades);
    }

    [Test]
    public void ShouldReturnFiveOfClubAs43thElement()
    {
        Deck testDeck = new Deck();
        Assert.AreEqual(testDeck.cards[43].Value, Value.Five);
        Assert.AreEqual(testDeck.cards[43].Suit, Suit.Clubs);
    }

    [Test]
    public void ShouldDrawAceOfSpadesBeforeShuffle()
    {
        Deck testDeck = new Deck();
        Card testCard = testDeck.DrawCard();
        Assert.AreEqual(testCard.Value, Value.Ace);
        Assert.AreEqual(testCard.Suit, Suit.Spades);
    }

    [Test]
    public void ShouldNotHaveSameCardsInTheSamePlaceAfterShuffle()
    {

        Deck testDeck = new Deck();
        Deck shuffledTestDeck = new Deck();
        int amountOfCardStayedAtSamePosition = 0;
        int nonAcceptableAmount = 1;

        shuffledTestDeck.Shuffle();

        for (int i = 0; i < testDeck.cards.Count; i++)
        {
            if (testDeck.cards[i].Value == shuffledTestDeck.cards[i].Value && testDeck.cards[i].Suit == shuffledTestDeck.cards[i].Suit)
                amountOfCardStayedAtSamePosition++;
        }

        Assert.LessOrEqual(amountOfCardStayedAtSamePosition, nonAcceptableAmount);
    }
}

